public enum ShipType {
    SHORT (2),
    TALL (3),
    GRANDE (4),
    VENTI (5);

    final int length;
    private ShipType(int length) {
        this.length = length;
    }
}
