import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class from which the entire program runs.
 * Also has some utilities for interfacing via command-line
 */
public class Main {

    /**
     * POJO for describing an attack.
     */
    private static class Attack {
        final int x;
        final int y;

        Attack(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    /**
     * Parses the input and tries to determine the player's attack.
     */
    private static Optional<Attack> parseInput(String s) {
        Pattern pattern = Pattern.compile("^[a-jA-J]([1-9]|10)$");
        Matcher matcher = pattern.matcher(s);
        if (!matcher.matches())
            return Optional.empty();

        char lChar = Character.toUpperCase(s.charAt(0)); // Letter
        String nString = s.substring(1); // Number
        int lNum = lChar - 'A' + 1;
        int nNum = Integer.parseInt(nString);
        return Optional.of(new Attack(nNum, lNum));
    }

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Game game = new Game();
        game.initializeGame();
        String input = "";
        boolean firstRound = true;
        boolean lastHumanAttackHit = false;
        boolean lastComputerAttackHit = false;
        Optional<Attack> parsedInput;

        while (!game.gameEnded()) {
            game.printGame(true);
            if (lastHumanAttackHit) {
                System.out.println("Congratulations! You hit the enemy's ship!");
            } else if (!firstRound) {
                System.out.println("Your attack missed the enemy's ship!");
            }
            if (lastComputerAttackHit) {
                System.out.println("The enemy has struck your ship!");
            } else if (!firstRound) {
                System.out.println("The enemy's attack missed your ships!");
            }

            parsedInput = Optional.empty();
            System.out.println("Please pick a combination [A-J][1-10] with no space in between.");

            while (!parsedInput.isPresent()) {
                input = reader.readLine();
                // Parse input
                parsedInput = parseInput(input);
                if (!parsedInput.isPresent()) {
                    System.out.println("Invalid input. Please pick a combination [A-J][1-10] with no space in between.");
                }
            }
            // Send attack
            Attack attack = parsedInput.get();
            lastHumanAttackHit = game.playerAttack(attack.x, attack.y);

            // Send CPU attack
            lastComputerAttackHit = game.computerAttack();

            // Round ending logic
            firstRound = false;
        }
        game.printGame(false);
        System.out.println("Thanks for playing! Come back soon!");
    }
}
