import java.util.Random;

class Game {
    private final static int B_HEIGHT = 11; // Includes labels
    private final static int B_WIDTH  = 11; // Includes labels
    private final static char C_PLR  = '@';
    private final static char C_CPU  = '#';
    private final static char C_EMPTY  = '_';
    private final static char C_HIT  = 'X';
    private final static Random random = new Random(); // consider passing this in.

    private char[][] plrBoard = new char[B_HEIGHT][B_WIDTH]; // player board
    private char[][] cpuBoard = new char[B_HEIGHT][B_WIDTH]; // computer board
    private boolean[][] cpuAttacks = new boolean[B_HEIGHT - 1][B_WIDTH - 1]; // computer's previous attacks

    /**
     * Initializes the chars for the given board.
     * The designatedChar represents the squares for ships.
     */
    private void initializeBoard(char[][] board, char designatedChar) {
        // Add labels
        board[0][0] = '\\';

        for (int i = 1; i < board[0].length; i++) {
            board[0][i] = (char) ('0' + i);
        }

        for (int i = 1; i < board.length; i++) {
            board[i][0] = (char) ('A' + i - 1);
        }

        // Setup all the ships.
        initializeShips(board, designatedChar);

        // Fill the board with empty;
        for (int i = 1; i < board.length; i++) {
            for (int j = 1; j < board[0].length; j++) {
                if (board[i][j] != designatedChar)
                    board[i][j] = C_EMPTY;
            }
        }
    }

    /**
     * Places all the ships for the given board.
     * The orientation of each attempt to place a given ship is random.
     */
    private void initializeShips(char[][] board, char designatedChar) {
        ShipType[] types = ShipType.values();
        for (ShipType type : types) {
            boolean shipPlaced  = false;
            while (!shipPlaced) {
                int x = 1 + random.nextInt(B_WIDTH - type.length - 1);
                int y = 1 + random.nextInt(B_HEIGHT - type.length - 1);
                boolean tryHorizontal = random.nextBoolean();

                // try one way
                if (placeShip(board, designatedChar, type.length, tryHorizontal, x, y)) {
                    shipPlaced = true;
                    continue;
                }

                // then try another way
                tryHorizontal = !tryHorizontal;
                shipPlaced = placeShip(board, designatedChar, type.length, tryHorizontal, x, y);
            }
        }
    }

    /**
     * Attempts to place a ship at the given coordinates and orientation.
     * If it determines that it can't place the ship, it returns false.
     * Otherwise, the ship is placed, and the method returns true.
     */
    private boolean placeShip(char[][] board, char designatedChar, int shipLength,
                              boolean horizontal, int x, int y) {
        int pos = (horizontal) ? x : y;
        for (int i = pos; i < pos + shipLength; i++) {
            int realX = (horizontal) ? i : x;
            int realY = (horizontal) ? y : i;
            if (board[realY][realX] == designatedChar) {
                return false;
            }
        }
        for (int i = pos; i < pos + shipLength; i++) {
            int realX = (horizontal) ? i : x;
            int realY = (horizontal) ? y : i;
            board[realY][realX] = designatedChar;
        }
        return true;
    }

    private boolean attack(char[][] board, char designatedChar, int x, int y) {
        if (board[y][x] == designatedChar) {
            board[y][x] = C_HIT;
            return true;
        }
        return false;
    }

    boolean playerAttack(int x, int y) {
        return attack(cpuBoard, C_CPU, x, y);
    }

    /**
     * Since the default value for boolean is false, we just set the corresponding boolean to true upon attack.
     * This way we prevent attacking the same square twice.
     */
    boolean computerAttack() {
        int x = -1;
        int y = -1;
        while (true) {
            x = 1 + random.nextInt(B_WIDTH - 1);
            y = 1 + random.nextInt(B_HEIGHT - 1);
            if (!cpuAttacks[y - 1][x - 1]) {
                break;
            }
        }
        cpuAttacks[y - 1][x - 1] = true;
        return attack(plrBoard, C_PLR, x, y);
    }

    void initializeGame() {
        initializeBoard(plrBoard, C_PLR);
        initializeBoard(cpuBoard, C_CPU);
    }

    private void printBoard(char[][] board, boolean covered) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                char c = board[i][j];
                if (covered && i != 0 && j != 0 && c != C_HIT) c = C_EMPTY;
                System.out.print(" " + c);
            }
            System.out.println();
        }
    }

    void printGame(boolean cpuCovered) {
        System.out.println("============================");
        System.out.println("Player Board:");
        printBoard(plrBoard, false);
        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
        System.out.println("Computer Board:");
        printBoard(cpuBoard, cpuCovered);
        // To see the entire cpu board.
        //printBoard(cpuBoard, false);
    }

    /**
     * Iterates through the given board to see if the designated char is present.
     * If there is not, then the ships are eliminated for the board.
     */
    private boolean shipsEliminated(char designatedChar, char[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if(designatedChar == board[i][j])
                    return false;
            }
        }
        return true;
    }

    /**
     * The game has ended when one of the players has lost all of their squares.
     * This could be simplified be keeping an int tally of how many squares each player has left.
     */
    boolean gameEnded() {
        return shipsEliminated(C_CPU, cpuBoard) || shipsEliminated(C_PLR, plrBoard);
    }
}
